//MENSAJES EMERGENTES
//alert("Diseño de aplicaciones web - U30");
//MENSAJES DE CONSOLA
console.log('Diseño de aplicaciones web - U30 - Desde consola');
//CREACION DE VARIABLES
var nombre;
//nombre = prompt("Ingresa tu nombre")
let apellido;
//apellido = prompt("Ingresa tu apellido")

//alert("El nombre del tripulante es: "+nombre+" "+apellido);

var tripulantes =["jerson","ivan","samuel"];
document.write(tripulantes);
document.write('<br><b>'+tripulantes[0]+" "+tripulantes[2]+'</b>');

tripulantes.push('thomas');
document.write('<br><b style="color:#333399">'+tripulantes+'</b>');

//FUNCIONES
function gestionaArray(parametro){
    document.write("<br>Ingreso a la funcion correctamente "+parametro);
}
gestionaArray('DESDE JS');

var suma=5+8;
gestionaArray(suma);

//CICLOS BUCLES O ITERACIONES
// forma 1
for(let i=0; i<7;i++){
    console.log(i);
}
document.write('<br><hr>');
// forma 2
for (let i=0; i<tripulantes.length; i++) {
    document.write('<br><b>'+tripulantes[i]+'</b>');   
}
document.write('<br><hr>');
// forma 3
for (let i=0; i<tripulantes.length; i++) {
    document.write(`<br><b> El Tripulante${i+1} es: ${tripulantes[i]}</b>`);   
}
document.write('<br><hr>');
// forma 4 IDEAL DE CONTRUCCION
for (var tripulante of tripulantes) {
    document.write(`<br><b> El Tripulante es: ${tripulante}</b>`);   
}
document.write('<br><hr>');
//MANEJO O IMPRESION DE LISTAS
//--> esta estructura es algo similar a lo que va a enviar y recibir el backend
let listaVuelos = [
    {"origen":"bucaramanga","destino":"cali","codVuelo":4527},
    {"origen":"bogota","destino":"barranquilla","codVuelo":6589},
    {"origen":"medellin","destino":"cartagena","codVuelo":8547},
    {
        "origen":"san andres",
        "destino":"pasto",
        "codVuelo":7536   
    }
];

//--> algo similar va hacer el frontend para visualizar los datos
document.write('<br><b><hr></b>');
document.write('<h1>ITINERARIO DE VUELOS</h1>');
for (var vuelo of listaVuelos) {
    document.write(`<br><hr><b> El vuelo:${vuelo.codVuelo} tiene origen: ${vuelo.origen} y destino ${vuelo.destino} </b>`);
}
document.write('<br><hr>');
// tabla de vuelo 
document.write('<br><b><hr></b>');
document.write('<h1>TABLA DE ITINERARIO DE VUELOS</h1>');
document.write('<table border="2">');
document.write('<tr>');
document.write('<th>vuelo</th>');
document.write('<th>origen</th>');
document.write('<th>destino</th>');
document.write('</tr>');
for (var vuelo of listaVuelos) {
    document.write(`<tr>
    <td> ${vuelo.codVuelo} </td>
    <td> ${vuelo.origen} </td>
    <td> ${vuelo.destino} </td>`);
}
document.write('</table>');
var datevuelos = document.getElementById("datavuelos");