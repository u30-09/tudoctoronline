import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import "./App.css";

//EMPLEADO
import CompEmpleadoCrear from "./components/empleado/EmpleadosCrear";
import CompEmpleadoEditar from "./components/empleado/EmpleadosEditar";
import CompEmpleadosListar from "./components/empleado/EmpleadosList";

// USUARIO
import CompUsuarioCrear from "./components/usuario/UsuariosCrear";
import CompUsuarioEditar from "./components/usuario/UsuariosEditar";
import CompUsuariosListar from "./components/usuario/UsuariosList";

//MEDICO
import CompMedicoCrear from "./components/medico/MedicoCrear";
import CompMedicoEditar from "./components/medico/MedicoEditar";
import CompMedicoListar from "./components/medico/MedicoList";

//CITA
import CompCitaCrear from "./components/cita/CitaCrear";
import CompcitaEditar from "./components/cita/CitaEditar";
import CompCitasListar from "./components/cita/CitaList";

import Navigator from "./components/Navigator";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Navigator />} />
        <Route path="/empleados" element={<CompEmpleadosListar />} />
        <Route path="/createEmpleado" element={<CompEmpleadoCrear />} />
        <Route path="/editEmpleado/:id" element={<CompEmpleadoEditar />} />

        <Route path="/usuarios" element={<CompUsuariosListar />} />
        <Route path="/createUsuario" element={<CompUsuarioCrear />} />
        <Route path="/editUsuario/:id" element={<CompUsuarioEditar />} />

        <Route path="/medico" element={<CompMedicoListar />} />
        <Route path="/createMedico" element={<CompMedicoCrear />} />
        <Route path="/editMedico/:id" element={<CompMedicoEditar />} />

        <Route path="/cita" element={<CompCitasListar />} />
        <Route path="/createCita" element={<CompCitaCrear />} />
        <Route path="/editCita/:id" element={<CompcitaEditar />} />
      </Routes>
    </Router>
  );
}

export default App;
