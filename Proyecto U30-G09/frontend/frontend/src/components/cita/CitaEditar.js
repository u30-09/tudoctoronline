import axios from "axios";
import { useState, useEffect } from "react";
import { useNavigate, useParams, Link } from "react-router-dom";

const URI = "http://localhost:4000/";

const CompCitaEditar = () => {
    // IMPLEMENTACION DE LOS HOOKS
    const [nombrePaciente, setNombre] = useState("");
    const [documentoPaciente, setDocumento] = useState("");
    const [especialidad, setEspecilidad] = useState("");
    const [medico, setMedico] = useState("");
    const [correo, setCorreo] = useState("");
    const [telefono, setTelefono] = useState("");
    const [fecha_cita, setFecha] = useState("");
    const [lugar, setLugar] = useState("");

    const navigate = useNavigate();
    const { id } = useParams();

    // METODO EDITAR
    const editar = async (e) => {
        e.preventDefault();
        await axios.put(`${URI}actualizarCita/${id}`, {
            nombrePaciente: nombrePaciente,
            documentoPaciente: documentoPaciente,
            especialidad: especialidad,
            medico: medico,
            correo: correo,
            telefono: telefono,
            fecha_cita: fecha_cita,
            lugar: lugar
        });
        navigate("/");
    };

    useEffect(() => {
        getUsuarioById();
    }, []);

    // METODO PARA BUSCAR EL EMPLEADO A EDITAR
    const getUsuarioById = async () => {
        const res = await axios.get(`${URI}consultaCi/${id}`);
        setNombre(res.data.message.nombrePaciente);
        setDocumento(res.data.message.documentoPaciente);
        setEspecilidad(res.data.message.especialidad);
        setMedico(res.data.message.medico);
        setCorreo(res.data.message.correo);
        setTelefono(res.data.message.telefono);
        setFecha(res.data.message.fecha_cita);
        setLugar(res.data.message.lugar);
    };

    return (
        <div>
            <h1>EDITAR CITA</h1>
            <form onSubmit={editar}>
                <div className="form-group">
                    <label className="form-label">Nombre Paciente: </label>
                    <input
                        value={nombrePaciente}
                        onChange={(e) => setNombre(e.target.value)}
                        type="text"
                        className="form-control"
                    />
                </div>
                <div className="form-group">
                    <label className="form-label">Documento Paciente: </label>
                    <input
                        value={documentoPaciente}
                        onChange={(e) => setDocumento(e.target.value)}
                        type="text"
                        className="form-control"
                    />
                </div>
                <div className="form-group">
                    <label className="form-label">Especialidad: </label>
                    <input
                        value={especialidad}
                        onChange={(e) => setEspecilidad(e.target.value)}
                        type="text"
                        className="form-control"
                    />
                </div>
                <div className="form-group">
                    <label className="form-label">Medico: </label>
                    <input
                        value={medico}
                        onChange={(e) => setMedico(e.target.value)}
                        type="text"
                        className="form-control"
                    />
                </div>
                <div className="form-group">
                    <label className="form-label">Correo: </label>
                    <input
                        value={correo}
                        onChange={(e) => setCorreo(e.target.value)}
                        type="text"
                        className="form-control"
                    />
                </div>
                <div className="form-group">
                    <label className="form-label">Telefono: </label>
                    <input
                        value={telefono}
                        onChange={(e) => setTelefono(e.target.value)}
                        type="text"
                        className="form-control"
                    />
                </div>
                <div className="form-group">
                    <label className="form-label">Fecha: </label>
                    <input
                        value={fecha_cita}
                        onChange={(e) => setFecha(e.target.value)}
                        type="text"
                        className="form-control"
                    />
                </div>
                <div className="form-group">
                    <label className="form-label">Lugar: </label>
                    <input
                        value={lugar}
                        onChange={(e) => setLugar(e.target.value)}
                        type="text"
                        className="form-control"
                    />
                </div>
                <button type="submit" className="btn btn-success mt-2 ml-2">
                    Crear
                </button>
                &nbsp;&nbsp;&nbsp;
                <Link to={"/"} className=" btn btn-success mt-2 ml-2">
                    Regresar
                </Link>
            </form>
        </div>
    );
};

export default CompCitaEditar;
