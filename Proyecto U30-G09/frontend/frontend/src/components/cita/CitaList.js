import axios from "axios";
// HOOKS
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

const URI = "http://localhost:4000/";

// CREACION DEL COMPONENTE
const CompCitasListar = () => {
  const [citas, setCita] = useState([]);
  useEffect(() => {
    getCitas();
  }, []);

  // METODOS DEL COMPONENTE
  // METODO PARA LISTAR LAS RUTAS DE VUELOS
  const getCitas = async () => {
    const res = await axios.get(`${URI}consultaCita/`);
    console.log(res.data);
    setCita(res.data.message);

    console.log("COLECCION_DATOS-->", res.data.result);
  };

  // METODO PARA ELIMINAR EMPLEADOS
  const deleteCitas = async (id) => {
    await axios.delete(`${URI}eliminarCita/${id}`);
    getCitas();
  };

  return (
    <div className="container">
      <div className="row">
        <div className="col">
          <h1>CITAS</h1>
          <Link
            to={"/createCita"}
            className=" btn btn-primary mt-2 ml-2 mb-1"
          >
            <i className="fa-solid fa-plus"></i>
          </Link>
          <table className="table table-dark table-striped">
            <thead className="table-primary">
              <tr>
                <th>Nombre</th>
                <th>Documento</th>
                <th>Especialidad</th>
                <th>Medico</th>
                <th>Correo</th>
                <th>Telefono</th>
                <th>Fecha</th>
                <th>Lugar</th>
              </tr>
            </thead>
            <tbody>
              {citas.map((cita) => (
                <tr key={cita._id}>
                  <td>{cita.nombrePaciente}</td>
                  <td>{cita.documentoPaciente}</td>
                  <td>{cita.especialidad}</td>
                  <td>{cita.medico}</td>
                  <td>{cita.correo}</td>
                  <td>{cita.telefono}</td>
                  <td>{cita.fecha_cita}</td>
                  <td>{cita.lugar}</td>
                  <td>
                    <Link
                      to={`/editCita/${cita._id}`}
                      className="fa-solid fa-pen-to-square"
                      style={{ color: "#fff" }}
                    ></Link>
                    &nbsp;&nbsp;&nbsp;
                    <Link
                      onClick={() => deleteCitas(cita._id)}
                      className="fa-solid fa-trash"
                      style={{ color: "#f65954" }}
                    ></Link>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
          &nbsp;&nbsp;&nbsp;
          <Link to={"/"} className=" btn btn-success mt-2 ml-2">
            Regresar
          </Link>
        </div>
      </div>
    </div>
  );
};

export default CompCitasListar;
