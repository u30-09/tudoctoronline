import axios from "axios";
// HOOKS
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

const URI = "http://localhost:4000/";

// CREACION DEL COMPONENTE
const CompMedicoListar = () => {
  const [medicos, setMedico] = useState([]);
  useEffect(() => {
    getMedico();
  }, []);

  // METODOS DEL COMPONENTE
  // METODO PARA LISTAR LAS RUTAS DE VUELOS
  const getMedico = async () => {
    const res = await axios.get(`${URI}buscarall/`);
    console.log(res.data);
    setMedico(res.data.message);

    console.log("COLECCION_DATOS-->", res.data.message);
  };

  // METODO PARA ELIMINAR EMPLEADOS
  const deleteMedico = async (id) => {
    await axios.delete(`${URI}medico/${id}`);
    getMedico();
  };

  return (
    <div className="container">
      <div className="row">
        <div className="col">
          <h1>Medico</h1>
          <Link
            to={"/createMedico"}
            className=" btn btn-primary mt-2 ml-2 mb-1"
          >
            <i className="fa-solid fa-plus"></i>
          </Link>
          <table className="table table-dark table-striped">
            <thead className="table-primary">
              <tr>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Documento</th>
                <th>Especialidad</th>
                <th>Cargo</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              {medicos.map((medico) => (
                <tr key={medico._id}>
                  <td>{medico.nombre}</td>
                  <td>{medico.apellido}</td>
                  <td>{medico.documento}</td>
                  <td>{medico.especialidad}</td>
                  <td>{medico.cargo}</td>
                  <td>
                    <Link
                      to={`/editMedico/${medico._id}`}
                      className="fa-solid fa-pen-to-square"
                      style={{ color: "#fff" }}
                    ></Link>
                    &nbsp;&nbsp;&nbsp;
                    <Link
                      onClick={() => deleteMedico(medico._id)}
                      className="fa-solid fa-trash"
                      style={{ color: "#f65954" }}
                    ></Link>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
          &nbsp;&nbsp;&nbsp;
          <Link to={"/"} className=" btn btn-success mt-2 ml-2">
            Regresar
          </Link>
        </div>
      </div>
    </div>
  );
};

export default CompMedicoListar;
