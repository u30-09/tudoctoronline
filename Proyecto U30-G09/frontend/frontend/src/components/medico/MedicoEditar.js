import axios from "axios";
import { useState, useEffect } from "react";
import { useNavigate, useParams, Link } from "react-router-dom";

const URI = "http://localhost:4000/";

const CompMedicoEditar = () => {
  // IMPLEMENTACION DE LOS HOOKS
  const [nombre, setNombre] = useState("");
  const [apellido, setApellido] = useState("");
  const [documento, setDocumento] = useState("");
  const [especialidad, setEspecialidad] = useState("");
  const [cargo, setCargo] = useState("");

  const navigate = useNavigate();
  const { id } = useParams();

  // METODO EDITAR
  const edit = async (e) => {
    e.preventDefault();
    await axios.put(`${URI}medico/${id}`, {
      nombre: nombre,
      apellido: apellido,
      documento: documento,
      especialidad: especialidad,
      cargo: cargo,
    });
    navigate("/");
  };

  useEffect(() => {
    getMedicoById();
  }, []);

  // METODO PARA BUSCAR EL EMPLEADO A EDITAR
  const getMedicoById = async () => {
    const res = await axios.get(`${URI}buscar/${id}`);
    setNombre(res.data.message.nombre);
    setApellido(res.data.message.apellido);
    setDocumento(res.data.message.documento);
    setEspecialidad(res.data.message.especialidad);
    setCargo(res.data.message.cargo);
  };

  return (
    <div>
      <h1>EDITAR MEDICO</h1>
      <form onSubmit={edit}>
      <div className="form-group">
          <label className="form-label">Nombre: </label>
          <input
            value={nombre}
            onChange={(e) => setNombre(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label className="form-label">Apellido: </label>
          <input
            value={apellido}
            onChange={(e) => setApellido(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label className="form-label">Documento: </label>
          <input
            value={documento}
            onChange={(e) => setDocumento(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label className="form-label">Especialidad: </label>
          <input
            value={especialidad}
            onChange={(e) => setEspecialidad(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label className="form-label">Cargo: </label>
          <input
            value={cargo}
            onChange={(e) => setCargo(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <button type="submit" className="btn btn-success mt-2 ml-2">
          Editar
        </button>
        &nbsp;&nbsp;&nbsp;
        <Link to={"/medico"} className=" btn btn-success mt-2 ml-2">
          Regresar
        </Link>
      </form>
    </div>
  );
};

export default CompMedicoEditar;
