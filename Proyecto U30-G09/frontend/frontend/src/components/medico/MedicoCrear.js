import axios from 'axios'
import { useState } from "react";
import { useNavigate } from "react-router-dom";

import { Link } from "react-router-dom";

const URI = "http://localhost:4000/crear";

const CompMedicoCrear = () => {
  // IMPLEMENTACION DE HOOKS
  const [nombre, setNombre] = useState("");
  const [apellido, setApellido] = useState("");
  const [documento, setDocumento] = useState("");
  const [especialidad, setEspecialidad] = useState("");
  const [cargo, setCargo] = useState("");

  const navigate = useNavigate();

  // METODO CREAR
  const createMedico = async (e) => {
    e.preventDefault();
    await axios.post(URI, {
      nombre: nombre,
      apellido: apellido,
      documento: documento,
      especialidad: especialidad,
      cargo: cargo,
    });
    navigate("/medico");
  };

  return (
    <div>
      <h1>CREACION DE MEDICO</h1>
      <form onSubmit={createMedico}>
        <div className="form-group">
          <label className="form-label">Nombre: </label>
          <input
            value={nombre}
            onChange={(e) => setNombre(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label className="form-label">Apellido: </label>
          <input
            value={apellido}
            onChange={(e) => setApellido(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label className="form-label">Documento: </label>
          <input
            value={documento}
            onChange={(e) => setDocumento(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label className="form-label">Especialidad: </label>
          <input
            value={especialidad}
            onChange={(e) => setEspecialidad(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label className="form-label">Cargo: </label>
          <input
            value={cargo}
            onChange={(e) => setCargo(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <button type="submit" className="btn btn-success mt-2 ml-2">
          Crear
        </button>
        &nbsp;&nbsp;&nbsp;
        <Link to={"/"} className=" btn btn-success mt-2 ml-2">
          Regresar
        </Link>
      </form>
    </div>
  );
};

export default CompMedicoCrear;
