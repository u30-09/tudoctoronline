import axios from "axios";
import { useState, useEffect } from "react";
import { useNavigate, useParams, Link } from "react-router-dom";

const URI = "http://localhost:4000/";

const CompUsuarioEditar = () => {
  // IMPLEMENTACION DE LOS HOOKS
  const [nombre, setNombre] = useState("");
  const [apellido, setApellido] = useState("");
  const [usuario, setUsuario] = useState("");
  const [clave, setClave] = useState("");
  const [cargo, setCargo] = useState("");

  const navigate = useNavigate();
  const { id } = useParams();

  // METODO EDITAR
  const editar = async (e) => {
    e.preventDefault();
    await axios.put(`${URI}actualizarUsuario/${id}`, {
      nombre: nombre,
      apellido: apellido,
      usuario: usuario,
      clave: clave,
      cargo: cargo
    });
    navigate("/");
  };

  useEffect(() => {
    getUsuarioById();
  }, []);

  // METODO PARA BUSCAR EL EMPLEADO A EDITAR
  const getUsuarioById = async () => {
    const res = await axios.get(`${URI}consultaUsua/${id}`);
    setNombre(res.data.message.nombre);
    setApellido(res.data.message.apellido);
    setUsuario(res.data.message.usuario);
    setClave(res.data.message.clave);
    setCargo(res.data.message.cargo);
  };

  return (
    <div>
      <h1>EDITAR USUARIO</h1>
      <form onSubmit={editar}>
        <div className="form-group">
          <label className="form-label">Nombre: </label>
          <input
            value={nombre}
            onChange={(e) => setNombre(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label className="form-label">Apellido: </label>
          <input
            value={apellido}
            onChange={(e) => setApellido(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label className="form-label">Usuario: </label>
          <input
            value={usuario}
            onChange={(e) => setUsuario(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label className="form-label">Clave: </label>
          <input
            value={clave}
            onChange={(e) => setClave(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label className="form-label">Cargo: </label>
          <input
            value={cargo}
            onChange={(e) => setCargo(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <button type="submit" className="btn btn-success mt-2 ml-2">
          Editar
        </button>
        &nbsp;&nbsp;&nbsp;
        <Link to={"/"} className=" btn btn-success mt-2 ml-2">
          Regresar
        </Link>
      </form>
    </div>
  );
};

export default CompUsuarioEditar;
