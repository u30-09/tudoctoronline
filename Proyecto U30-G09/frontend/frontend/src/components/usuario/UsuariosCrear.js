import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

import { Link } from "react-router-dom";

const URI = "http://localhost:4000/guardarUsuario";

const CompUsuarioCrear = () => {
  // IMPLEMENTACION DE HOOKS
  const [nombre, setNombre] = useState("");
  const [apellido, setApellido] = useState("");
  const [usuario, setUsuario] = useState("");
  const [clave, setClave] = useState("");
  const [cargo, setCargo] = useState("");

  const navigate = useNavigate();

  // METODO CREAR
  const create = async (e) => {
    e.preventDefault();
    await axios.post(URI, {
      nombre: nombre,
      apellido: apellido,
      usuario: usuario,
      clave: clave,
      cargo: cargo
    });
    navigate("/usuarios");
  };

  return (
    <div>
      <h1>CREACION DE USUARIO</h1>
      <form onSubmit={create}>
        <div className="form-group">
          <label className="form-label">Nombre: </label>
          <input
            value={nombre}
            onChange={(e) => setNombre(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label className="form-label">Apellido: </label>
          <input
            value={apellido}
            onChange={(e) => setApellido(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label className="form-label">Usuario: </label>
          <input
            value={usuario}
            onChange={(e) => setUsuario(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label className="form-label">Clave: </label>
          <input
            value={clave}
            onChange={(e) => setClave(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label className="form-label">Cargo: </label>
          <input
            value={cargo}
            onChange={(e) => setCargo(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <button type="submit" className="btn btn-success mt-2 ml-2">
          Crear
        </button>
        &nbsp;&nbsp;&nbsp;
        <Link to={"/"} className=" btn btn-success mt-2 ml-2">
          Regresar
        </Link>
      </form>
    </div>
  );
};

export default CompUsuarioCrear;
