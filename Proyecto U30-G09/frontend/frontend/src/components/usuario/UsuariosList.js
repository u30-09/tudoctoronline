import axios from "axios";
// HOOKS
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

const URI = "http://localhost:4000/";

// CREACION DEL COMPONENTE
const CompUsuariosListar = () => {
  const [usuarios, setUsuario] = useState([]);
  useEffect(() => {
    getUsuarios();
  }, []);

  // METODOS DEL COMPONENTE
  // METODO PARA LISTAR LAS RUTAS DE VUELOS
  const getUsuarios = async () => {
    const res = await axios.get(`${URI}consultaUsuario/`);
    console.log(res.data);
    setUsuario(res.data.message);

    console.log("COLECCION_DATOS-->", res.data.message);
  };

  // METODO PARA ELIMINAR EMPLEADOS
  const deleteUsuarios = async (id) => {
    await axios.delete(`${URI}eliminarUsuario/${id}`);
    getUsuarios();
  };

  return (
    <div className="container">
      <div className="row">
        <div className="col">
          <h1>USUARIOS</h1>
          <Link
            to={"/createUsuario"}
            className=" btn btn-primary mt-2 ml-2 mb-1"
          >
            <i className="fa-solid fa-plus"></i>
          </Link>
          <table className="table table-dark table-striped">
            <thead className="table-primary">
              <tr>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Usuario</th>
                <th>Clave</th>
                <th>Cargo</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              {usuarios.map((usuario) => (
                <tr key={usuario._id}>
                  <td>{usuario.nombre}</td>
                  <td>{usuario.apellido}</td>
                  <td>{usuario.usuario}</td>
                  <td>{usuario.clave}</td>
                  <td>{usuario.cargo}</td>
                  <td>
                    <Link
                      to={`/editUsuario/${usuario._id}`}
                      className="fa-solid fa-pen-to-square"
                      style={{ color: "#fff" }}
                    ></Link>
                    &nbsp;&nbsp;&nbsp;
                    <Link
                      onClick={() => deleteUsuarios(usuario._id)}
                      className="fa-solid fa-trash"
                      style={{ color: "#f65954" }}
                    ></Link>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
          &nbsp;&nbsp;&nbsp;
          <Link to={"/"} className=" btn btn-success mt-2 ml-2">
            Regresar
          </Link>
        </div>
      </div>
    </div>
  );
};

export default CompUsuariosListar;
