import axios from "axios";
import { useState, useEffect } from "react";
import { useNavigate, useParams, Link } from "react-router-dom";

const URI = "https://backend-doc-online.herokuapp.com/";

const CompEmpleadoEditar = () => {
  // IMPLEMENTACION DE LOS HOOKS
  const [nombre, setNombre] = useState("");
  const [apellido, setApellido] = useState("");
  const [documento, setDocumento] = useState("");
  const [cargo, setCargo] = useState("");

  const navigate = useNavigate();
  const { id } = useParams();

  // METODO EDITAR
  const edit = async (e) => {
    e.preventDefault();
    await axios.put(`${URI}actualizarEmpleado/${id}`, {
      nombre: nombre,
      apellido: apellido,
      documento: documento,
      cargo: cargo,
    });
    navigate("/empleados");
  };

  useEffect(() => {
    getEmpleadoById();
  }, []);

  // METODO PARA BUSCAR EL EMPLEADO A EDITAR
  const getEmpleadoById = async () => {
    const res = await axios.get(`${URI}consulta/${id}`);
    setNombre(res.data.result.nombre);
    setApellido(res.data.result.apellido);
    setDocumento(res.data.result.documento);
    setCargo(res.data.result.cargo);
  };

  return (
    <div>
      <h1>EDITAR EMPLEADO</h1>
      <form onSubmit={edit}>
        <div className="form-group">
          <label className="form-label">Nombre: </label>
          <input
            value={nombre}
            onChange={(e) => setNombre(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label className="form-label">Apellido: </label>
          <input
            value={apellido}
            onChange={(e) => setApellido(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label className="form-label">Documento: </label>
          <input
            value={documento}
            onChange={(e) => setDocumento(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label className="form-label">Cargo: </label>
          <input
            value={cargo}
            onChange={(e) => setCargo(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <button type="submit" className="btn btn-success mt-2 ml-2">
          Editar
        </button>
        &nbsp;&nbsp;&nbsp;
        <Link to={"/empleados"} className=" btn btn-success mt-2 ml-2">
          Regresar
        </Link>
      </form>
    </div>
  );
};

export default CompEmpleadoEditar;
