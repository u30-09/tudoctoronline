import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

import { Link } from "react-router-dom";

const URI = "https://backend-doc-online.herokuapp.com/registro";

const CompEmpleadoCrear = () => {
  // IMPLEMENTACION DE HOOKS
  const [nombre, setNombre] = useState("");
  const [apellido, setApellido] = useState("");
  const [documento, setDocumento] = useState("");
  const [cargo, setCargo] = useState("");

  const navigate = useNavigate();

  // METODO CREAR
  const create = async (e) => {
    e.preventDefault();
    await axios.post(URI, {
      nombre: nombre,
      apellido: apellido,
      documento: documento,
      cargo: cargo,
    });
    navigate("/empleados");
    alert("Creación de empleado exitosa");
  };

  return (
    <div>
      <h1>CREACION DE EMPLEADO</h1>
      <form onSubmit={create}>
        <div className="form-group">
          <label className="form-label">Nombre: </label>
          <input
            value={nombre}
            onChange={(e) => setNombre(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label className="form-label">Apellido: </label>
          <input
            value={apellido}
            onChange={(e) => setApellido(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label className="form-label">Documento: </label>
          <input
            value={documento}
            onChange={(e) => setDocumento(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label className="form-label">Cargo: </label>
          <input
            value={cargo}
            onChange={(e) => setCargo(e.target.value)}
            type="text"
            className="form-control"
          />
        </div>
        <button type="submit" className="btn btn-success mt-2 ml-2">
          Crear
        </button>
        &nbsp;&nbsp;&nbsp;
        <Link to={"/"} className=" btn btn-success mt-2 ml-2">
          Regresar
        </Link>
      </form>
    </div>
  );
};

export default CompEmpleadoCrear;
