import axios from "axios";
// HOOKS
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

const URI = "https://backend-doc-online.herokuapp.com/";

// CREACION DEL COMPONENTE
const CompEmpleadosListar = () => {
  const [empleados, setEmpleado] = useState([]);
  useEffect(() => {
    getEmpleados();
  }, []);

  // METODOS DEL COMPONENTE
  // METODO PARA LISTAR LAS RUTAS DE VUELOS
  const getEmpleados = async () => {
    const res = await axios.get(`${URI}consultaAll/`);
    console.log(res.data);
    setEmpleado(res.data.result);

    console.log("COLECCION_DATOS-->", res.data.result);
  };

  // METODO PARA ELIMINAR EMPLEADOS
  const deleteEmpleados = async (id) => {
    await axios.delete(`${URI}eliminarEmpleado/${id}`);
    getEmpleados();
    alert("Registro eliminado con exito");
  };

  return (
    <div className="container">
      <div className="row">
        <div className="col">
          <h1>Empleados</h1>
          <Link
            to={"/createEmpleado"}
            className=" btn btn-primary mt-2 ml-2 mb-1"
          >
            <i className="fa-solid fa-plus"></i>
          </Link>
          <table className="table table-dark table-striped">
            <thead className="table-primary">
              <tr>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Documento</th>
                <th>Rol</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              {empleados.map((empleado) => (
                <tr key={empleado._id}>
                  <td>{empleado.nombre}</td>
                  <td>{empleado.apellido}</td>
                  <td>{empleado.documento}</td>
                  <td>{empleado.cargo}</td>
                  <td>
                    <Link
                      to={`/editEmpleado/${empleado._id}`}
                      className="fa-solid fa-pen-to-square"
                      style={{ color: "#fff" }}
                    ></Link>
                    &nbsp;&nbsp;&nbsp;
                    <Link
                      onClick={() => deleteEmpleados(empleado._id)}
                      className="fa-solid fa-trash"
                      style={{ color: "#f65954" }}
                    ></Link>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
          &nbsp;&nbsp;&nbsp;
          <Link to={"/"} className=" btn btn-success mt-2 ml-2">
            Regresar
          </Link>
        </div>
      </div>
    </div>
  );
};

export default CompEmpleadosListar;
