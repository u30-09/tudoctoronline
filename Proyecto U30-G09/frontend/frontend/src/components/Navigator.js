import { Link } from "react-router-dom";

const Navigator = () => {
  return (
    <>
      <header className="top-header">
        <div className="container-fluid row">
          <h2 className="row">DocOnline</h2>
          <ul className="nav justify-content-justify-content-sm-between">
            <li className="nav-item">
              <Link
                to={"/empleados"}
                className=" btn btn-primary mt-2 ml-2 mb-1"
              >
                EMPLEADOS
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/medico"} className=" btn btn-primary mt-2 ml-2 mb-1">
                MEDICO
              </Link>
            </li>
            <li className="nav-item">
              <Link
                to={"/usuarios"}
                className=" btn btn-primary mt-2 ml-2 mb-1"
              >
                USUARIOS
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/citas"} className=" btn btn-primary mt-2 ml-2 mb-1">
                CITA
              </Link>
            </li>
          </ul>
        </div>
      </header>
    </>
  );
};

export default Navigator;
