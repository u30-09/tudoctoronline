//CARGA DE DEPENDENCIAS
var express = require("express");
var app = express();

// CARGA Y USO DE LOS CORS SE DEBEN INSTALAR ANTES
const cors = require("cors");
app.use(cors());

var bodyparser = require("body-parser");
//var methodOverride =require("method-override");
var mongoose = require("mongoose");

//USO DE DEPNDENCIAS
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

//CONFIGURACION DE LA CABECERA Y CORS
app.use((req, res, next) => {
  res.header("Access-Control-Allow-origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Authorization, X-API-KEY, XRequested-with, Content-Type, Accept,Access-Control-Allow-Request-Method"
  );
  res.header(
    "Access-Control-Allow-Request-Method",
    "GET,POST,PUT,DELETE,OPTIONS"
  );
  res.header("Allow", "GET,POST,PUT,DELETE,OPTIONS");
  next();
});

//RUTAS
app.use(require("./routers/routersEmpleado"));
app.use(require("./routers/routersUsuario"));
app.use(require("./routers/routersMedico"));
app.use(require("./routers/routersCita"));
module.exports = app;
