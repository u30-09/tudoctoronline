const { Router } = require("express");
const router = Router();

// RUTAS EMPLEADOS
var controllerEmpleado = require("../src/controllers/controllEmpleado");
router.get("/prueba", controllerEmpleado.prueba);
router.post("/registro", controllerEmpleado.saveEmpleado);
router.get("/consulta/:id", controllerEmpleado.searchData);
router.get("/consultaAll/:id?", controllerEmpleado.listAllData);
router.put("/actualizarEmpleado/:id", controllerEmpleado.updateEmpleado);
router.delete("/eliminarEmpleado/:id", controllerEmpleado.deleteEmpleado);

module.exports = router;
