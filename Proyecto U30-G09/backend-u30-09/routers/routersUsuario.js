const { Router } = require("express");
const router = Router();

// RUTAS USUARIOS
var controlerUsuario = require("../src/controllers/controlUsuario");
router.get("/pruebaUsuario", controlerUsuario.pruebaUsuario);
router.post("/guardarUsuario", controlerUsuario.saveUsuario);
router.get("/consultaUsuario/:id?", controlerUsuario.listAllDataUsuario);
router.get("/consultaUsua/:id", controlerUsuario.buscarId);
router.put("/actualizarUsuario/:id", controlerUsuario.updateUsuario);
router.delete("/eliminarUsuario/:id", controlerUsuario.deleteUsuario);

module.exports = router;
