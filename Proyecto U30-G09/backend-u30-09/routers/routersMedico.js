const {Router} = require('express');
const router = Router();


var controllerMedico = require('../src/controllers/controllerMedico');
//localhost:4000/prueba
router.get('/prueba',controllerMedico.prueba);
//localhost:4000/crear
router.post('/crear',controllerMedico.saveMedico);
//localhost:4000/buscar/
//numero id que quiero buscar
router.get('/buscar/:id',controllerMedico.buscarData);
//localhost:4000/buscarall/
//numero id que quiero buscar
router.get('/buscarall/:id?',controllerMedico.listarAllData);
//localhost:4000/medico/actualizar
router.put('/medico/:id',controllerMedico.updateMedico);
//localhost:4000/medico/eliminar
router.delete('/medico/:id',controllerMedico.deleteMedico);

module.exports = router;