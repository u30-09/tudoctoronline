const { Router } = require("express");
const router = Router();

// RUTAS USUARIOS
var controlerCita = require("../src/controllers/controlCita");
router.get("/pruebaCita", controlerCita.pruebaCita);
router.post("/guardarCita", controlerCita.saveCita);
router.get("/consultaCita/:id?", controlerCita.listAllCita);
router.get("/consultaCi/:id", controlerCita.buscarCitaId);
router.put("/actualizarCita/:id", controlerCita.updateCita);
router.delete("/eliminarCita/:id", controlerCita.deleteCita);

module.exports = router;
