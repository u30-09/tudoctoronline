const Medico = require("../model/medico");

function prueba (req,res){
    res.status(200).send({
        message:'probando la ejecucion del controlador - tudoctor(medico) U30-09'
    })    
}

function saveMedico(req,res){
    var myMedico =new Medico(req.body);
    //FUNCION DE TIPO PROMESA
    myMedico.save((err,result) => {
        res.status(200).send({message:result});
    });
}

function buscarData(req,res){
    var idMedico = req.params.id;
    Medico.findById(idMedico).exec((err,result)=>{
        if(err){
            res.status(500).send({message:'Error al momento de procesar en el backend U30-09'});
        }else{
            if(!result){
                res.status(404).send({message:'Se ha ingresado valores incorrectos y no se puede procesar'});
            }else{
                res.status(200).send({message:result});
            }
        }
    })
}

function listarAllData(req,res){
    var idMedico = req.params.id;
    if(!idMedico){
        var result = Medico.find({}).sort('Documento');
    }else{
        var result = Medico.find({_id:idMedico}).sort('Documento')
    }
    result.exec(function(err,result){
        if(err){
            res.status(500).send({message:'Error al momento de procesar en el backend'});
        }else{
            if(!result){
                res.status(404).send({message:'Se ha ingresado valores incorrectos y no se puede procesar'});
            }else{
                res.status(200).send({message:result});
            }
        }
    })
}

function updateMedico(req,res){
    var idMedico = req.params.id;
    Medico.findByIdAndUpdate({_id:idMedico},req.body,{new:true},function(err,Medico){
        if(err)
            res.send(err)
        res.json(Medico)
    });
}

function deleteMedico(req,res){
    var idMedico = req.params.id;
    Medico.findByIdAndDelete(idMedico,function(err,Medico){
        if(err){
            return res.json(500,{
                message:'No se ha encontrado la ruta a eliminar'
            })
        }
        return res.json(Medico);
    })
}

module.exports = {
    prueba,
    saveMedico,
    buscarData,
    listarAllData,
    updateMedico,
    deleteMedico
};