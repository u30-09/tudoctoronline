const Cita = require("../model/cita");

function pruebaCita(req, res) {
  res.status(200).send({
    message: "Probando cita",
  });
}

// METODOS CRUD
function saveCita(req, res) {
  var cita = new Cita(req.body);
  cita.save((err, result) => {
    res.status(200).send({ message: result, message: "La cita se registro con exito" });
  });
}

function listAllCita(req, res) {
  var idCita = req.params.id;
  if (!idCita) {
    var result = Cita.find({}).sort("nombrePaciente");
  } else {
    var result = Cita.find({ _id: idCita }).sort("nombrePaciente");
  }

  result.exec(function (err, result) {
    if (err) {
      res.status(500).send({ message: "Error al momento de procesar en el backend" });
    } else {
      if (!result) {
        res.status(404).send({
          message: "Se ha ingresado valores incorrectos y no se puede procesar",
        });
      } else {
        res.status(200).send({ message: result });
      }
    }
  });
}

function buscarCitaId(req, res) {
  var idCita = req.params.id;
  Cita.findById(idCita).exec((err, result) => {
    if (err) {
      res
        .status(500)
        .send({ message: "Error al momento de procesar en el backend" });
    } else {
      if (!result) {
        res.status(4000).send({
          message: "Se ha ingresado valores incorrectos y no se puede procesar",
        });
      } else {
        res.status(200).send({ message:result });
      }
    }
  });
}

function updateCita(req, res) {
  var idCita = req.params.id;
  Cita.findByIdAndUpdate(
    { _id: idCita },
    req.body,
    { new: true },
    function (err, Citas) {
      if (err) {
        return res.json(500, {
          message: "No se ha encontrado la cita",
        });
      }
      return res.json(Cita);
    });
  }

function deleteCita(req, res) {
  var idCita = req.params.id;
  Cita.findByIdAndRemove({ _id: idCita }, function (err, Rutas) {
    if (err) {
      return res.json(500, {
        message: "No se ha encontrado la cita",
      });
    }
    return res
      .send({ message: "La cita se elimino con exito" })
      .json(Cita);
  });
}

module.exports = {
  pruebaCita,
  saveCita,
  listAllCita,
  updateCita,
  deleteCita,
  buscarCitaId
};
