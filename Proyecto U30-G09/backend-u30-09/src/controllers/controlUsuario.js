const Usuario = require("../model/usuario");

function pruebaUsuario(req, res) {
  res.status(200).send({
    message: "Probando",
  });
}

// METODOS CRUD
function saveUsuario(req, res) {
  var usuario = new Usuario(req.body);
  usuario.save((err, result) => {
    res.status(200).send({ message: result, message: "El usuario se registro con exito" });
  });
}

function listAllDataUsuario(req, res) {
  var idUsuario = req.params.id;
  if (!idUsuario) {
    var result = Usuario.find({}).sort("nombre");
  } else {
    var result = Usuario.find({ _id: idUsuario }).sort("nombre");
  }

  result.exec(function (err, result) {
    if (err) {
      res.status(500).send({ message: "Error al momento de procesar en el backend" });
    } else {
      if (!result) {
        res.status(404).send({
          message: "Se ha ingresado valores incorrectos y no se puede procesar",
        });
      } else {
        res.status(200).send({ message: result });
      }
    }
  });
}

function buscarId(req, res) {
  var idUsuario = req.params.id;
  Usuario.findById(idUsuario).exec((err, result) => {
    if (err) {
      res
        .status(500)
        .send({ message: "Error al momento de procesar en el backend" });
    } else {
      if (!result) {
        res.status(4000).send({
          message: "Se ha ingresado valores incorrectos y no se puede procesar",
        });
      } else {
        res.status(200).send({ message:result });
      }
    }
  });
}

function updateUsuario(req, res) {
  var idUsuario = req.params.id;
  Usuario.findByIdAndUpdate(
    { _id: idUsuario },
    req.body,
    { new: true },
    function (err, Rutas) {
      if (err) res.send(err);
      res
        .send({ message: "El usuario se actualizo con exito" })
        .json(Usuario);
    }
  );
}

function deleteUsuario(req, res) {
  var idUsuario = req.params.id;
  Usuario.findByIdAndRemove({ _id: idUsuario }, function (err, Rutas) {
    if (err) {
      return res.json(500, {
        message: "No se ha encontrado el usuario",
      });
    }
    return res
      .send({ message: "El usuario se elimino con exito" })
      .json(Usuario);
  });
}

module.exports = {
  pruebaUsuario,
  saveUsuario,
  listAllDataUsuario,
  updateUsuario,
  deleteUsuario,
  buscarId
};
