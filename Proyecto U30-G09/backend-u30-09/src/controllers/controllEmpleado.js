const Empleado = require("../model/empleado");

function prueba(req, res) {
  res.status(200).send({
    message: "Probando la ejecuacion del controlador",
  });
}

// METODOS CRUD
function saveEmpleado(req, res) {
  var empleado = new Empleado(req.body);
  empleado.save((err, result) => {
    res.status(200).send({ result });
  });
}

function searchData(req, res) {
  var idEmpleado = req.params.id;
  Empleado.findById(idEmpleado).exec((err, result) => {
    if (err) {
      res
        .status(500)
        .send({ message: "Error al momento de procesar en el backend" });
    } else {
      if (!result) {
        res.status(4000).send({
          message: "Se ha ingresado valores incorrectos y no se puede procesar",
        });
      } else {
        res.status(200).send({ result });
      }
    }
  });
}

function listAllData(req, res) {
  var idEmpleado = req.params.id;
  if (!idEmpleado) {
    var result = Empleado.find({}).sort("Nombre");
  } else {
    var result = Empleado.find({ _id: idEmpleado }).sort("Nombre");
  }

  result.exec(function (err, result) {
    if (err) {
      res
        .status(500)
        .send({ message: "Error al momento de procesar en el backend" });
    } else {
      if (!result) {
        res.status(404).send({
          message: "Se ha ingresado valores incorrectos y no se puede procesar",
        });
      } else {
        res.status(200).send({ result });
      }
    }
  });
}

function updateEmpleado(req, res) {
  var idEmpleado = req.params.id;
  Empleado.findByIdAndUpdate(
    { _id: idEmpleado },
    req.body,
    { new: true },
    function (err, Empleado) {
      if (err) res.send(err);
      res.json(Empleado);
    }
  );
}

function deleteEmpleado(req, res) {
  var idEmpleado = req.params.id;
  Empleado.findByIdAndRemove({ _id: idEmpleado }, function (err, Empleado) {
    if (err) {
      return res.json(500, {
        message: "No se ha encontrado el empleado",
      });
    }
    return res.json(Empleado);
  });
}

module.exports = {
  prueba,
  saveEmpleado,
  searchData,
  listAllData,
  updateEmpleado,
  deleteEmpleado,
};
