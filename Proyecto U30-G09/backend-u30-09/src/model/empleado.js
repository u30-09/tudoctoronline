var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var empleadoSchema = Schema({
  nombre: String,
  apellido: String,
  documento: String,
  cargo: String,
});

const Empleado = mongoose.model("tbc_empleado", empleadoSchema);
module.exports = Empleado;
