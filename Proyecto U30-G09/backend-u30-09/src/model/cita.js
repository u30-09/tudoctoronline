var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var rutaSchema = Schema({
    nombrePaciente:String,
    documentoPaciente:String,
    especialidad:String,
    medico:String,
    correo:String,
    telefono:String,
    fecha_cita:Date,
    lugar:String
});

const Cita = mongoose.model('tbc_cita',rutaSchema);
module.exports = Cita;