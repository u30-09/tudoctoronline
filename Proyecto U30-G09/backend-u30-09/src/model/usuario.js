var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var rutaSchema = Schema({
    nombre:String,
    apellido:String,
    usuario:String,
    clave:String,
    cargo:String
});

const Usuario = mongoose.model('tbc_usuario',rutaSchema);
module.exports = Usuario;