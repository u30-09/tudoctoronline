var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var medicoSchema = Schema({
  nombre: String,
  apellido: String,
  documento: String,
  especialidad: String,
  cargo: String,
});
const Medico = mongoose.model("tbc_medico", medicoSchema);
module.exports = Medico;
